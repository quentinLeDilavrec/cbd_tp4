CBD TP4 
=====================================

.. code-block:: python

    cd $PROJET_DIR
    python3 -m virtualenv env_cbd_tp4
    
    source env_cbd_tp4/bin/activate
    git clone https://gitlab.com/quentinLeDilavrec/cbd_tp4.git cbd_tp4
    cd cbd_tp4
    pip install -e .
    $PROJET_DIR/env_cbd_tp4/bin/cbd_tp4 --help
    $PROJET_DIR/env_cbd_tp4/bin/cbd_tp4 deploy g5k --env enos_env_g5k
    $PROJET_DIR/env_cbd_tp4/bin/cbd_tp4 vdeploy \
        --prec-env enos_env_g5k --env enos_env_vbasi
