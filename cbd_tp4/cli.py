import click
import logging
import yaml

import cbd_tp4.tasks as t
from cbd_tp4.constants import CONF

logging.basicConfig(level=logging.DEBUG)


@click.group()
def cli():
    pass


def load_config(file_path):
    """
    Read configuration from a file in YAML format.
    :param file_path: Path of the configuration file.
    :return:
    """
    with open(file_path) as f:
        configuration = yaml.safe_load(f)
    return configuration


@cli.command(help="Claim resources on Grid'5000 (frontend).")
@click.option("--force",
              is_flag=True,
              help="force redeployment")
@click.option("--conf",
              default=CONF,
              help="alternative configuration file")
@click.option("--env",
              help="alternative environment directory")
def vmong5k(force, conf, env):
    config = load_config(conf)
    t.vmong5k(config, force, env=env)


@cli.command(help="Claim resources on Grid'5000 (frontend).")
@click.option("--force",
              is_flag=True,
              help="force redeployment")
@click.option("--conf",
              default=CONF,
              help="alternative configuration file")
@click.option("--env",
              help="alternative environment directory")
def g5k(force, conf, env):
    config = load_config(conf)
    t.g5k(config, force, env=env)


@cli.command(help="Claim resources on vagrant (localhost).")
@click.option("--force",
              is_flag=True,
              help="force redeployment")
@click.option("--conf",
              default=CONF,
              help="alternative configuration file")
@click.option("--env",
              help="alternative environment directory")
def vagrant(force, conf, env):
    config = load_config(conf)
    t.vagrant(config, force, env=env)


@cli.command(help="Generate the Ansible inventory [after g5k or vagrant].")
@click.option("--env",
              help="alternative environment directory")
def inventory(env):
    t.inventory(env=env)


@cli.command(help="Configure available resources [after deploy, inventory or\
             destroy].")
@click.option("--env",
              help="alternative environment directory")
def prepare(env):
    t.prepare(env=env)


@cli.command(help="Backup the deployed environment")
@click.option("--env",
              help="alternative environment directory")
def backup(env):
    t.backup(env=env)


@cli.command(help="Destroy the deployed environment")
@click.option("--env",
              help="alternative environment directory")
def destroy(env):
    t.destroy(env=env)


@cli.command(help="Claim resources from a PROVIDER and configure them.")
@click.argument("provider")
@click.option("--force",
              is_flag=True,
              help="force redeployment")
@click.option("--conf",
              default=CONF,
              help="alternative configuration file")
@click.option("--env",
              help="alternative environment directory")
def deploy(provider, force, conf, env):
    config = load_config(conf)
    t.PROVIDERS[provider](config, force, env=env)
    t.inventory()
    t.prepare(env=env)


@cli.command(help="Instanciate ressources like vms and container on already reserved resources.")
@click.option("--force",
              is_flag=True,
              help="force redeployment")
@click.option("--conf",
              default=CONF,
              help="alternative configuration file")
@click.option("--env",
              help="alternative environment directory")
@click.option("--prev-env",
              help="alternative previous environment directory")
@click.option("--stack",
              is_flag=True,
              help="stack a new virtual layer on top of another")
@click.option('-s','--spreading', 
              multiple=True,
              help='''vms/cont to be started on node type
<role of underlayer>:<any entity installable on pm>,...
example.
  type0:vm1,vm1,vm0,cont1''')
@click.option("--shift",
              default=0,
              help="shift subnet start")
def vdeploy(force, conf, env, prev_env, stack, spreading, shift):
    config = load_config(conf)
    prev_env = t.reload_env(force, env=prev_env)
    
    spreading = [(one.split(':',1)[0],one.split(':',1)[1].split(',')) for one in spreading]

    t.virtual_layer(config, force, prev_env, spreading, env=env, shift = int(shift))
    
    
@cli.command(help="Bench link between nodes")
@click.option("--env",
              help="alternative environment directory")
@click.option("-n",
              multiple=True,
              help="nodes to test")
def bench_link(env, n):
    res1 = t.iperf(s=n[0], c=n[1:], env=env)
    res2 = t.ping(t=n[0], s=n[1:], env=env)
    print(res1, res2)


@cli.command(help="Bench with hanoi")
@click.option("--env",
              help="alternative environment directory")
@click.option("-n",
              multiple=True,
              help="nodes to test")
def bench_hanoi(env, n):
    res1 = t.hanoi(l=n, env=env)
    print(res1)


@cli.command(help="Bench overcommit")
@click.option("--env",
              help="alternative environment directory")
@click.option("--prev-env",
              help="alternative previous environment directory")
@click.option("--to",
              help="increase choosed variable until >to")
@click.option("--cores",
              help="force redeployment")
@click.option("--mem",
              help="force redeployment")
@click.option("--number",
              help="force redeployment")
def bench_overcommit(env, prev_env, to, **kwargs):
    if "cores" not in kwargs:
        var = "cores"
    elif "mem" not in kwargs:
        var = "mem"
    elif "number" not in kwargs:
        var = "number"
    else:
        return -1
    config = load_config(conf)
    prev_env = t.reload_env(force, env=prev_env)
    for v in range(int(to)):
        spreading = [("type0",["vm0" if vm else "cont0" for _ in range(v)]),
                ("type1",["vm1","cont1"])]
        tmp_env = t.virtual_layer(config, force, prev_env, spreading, env=env)
        res1 = t.hanoi(l=None, env=tmp_env["resultdir"])
        print(res1)
