from enoslib.api import generate_inventory, run_ansible
from enoslib.task import enostask
from enoslib.infra.enos_g5k.provider import G5k
from enoslib.infra.enos_g5k.configuration import Configuration as G5kConf
from enoslib.infra.enos_vagrant.provider import Enos_vagrant
from enoslib.infra.enos_vagrant.configuration import Configuration as VagrantConf
from enoslib.infra.enos_vmong5k.constants import DEFAULT_IMAGE
from enoslib.infra.enos_vmong5k.provider import VMonG5k, VirtualMachine, mac_range
from enoslib.infra.enos_vmong5k.configuration import Configuration as VMonG5kConf
import logging
import os

from netaddr import EUI, IPAddress, mac_unix_expanded

from cbd_tp4.constants import ANSIBLE_DIR

import uuid

logger = logging.getLogger(__name__)


@enostask(new=True)
def vmong5k(config, force, env=None, **kwargs):
    conf = VMonG5kConf.from_dictionnary(config["vmong5k"])
    from pprint import pprint
    pprint(conf.to_dict())
    provider = VMonG5k(conf)
    roles, networks = provider.init(force_deploy=force)
    env["config"] = config
    env["roles"] = roles
    env["networks"] = networks
    env["provider"] = "vmong5k"
    return env


@enostask(new=True)
def g5k(config, force, env=None, **kwargs):
    conf = G5kConf.from_dictionnary(config["g5k"])
    provider = G5k(conf)
    roles, networks = provider.init(force_deploy=force)
    env["config"] = config
    env["roles"] = roles
    env["networks"] = networks
    env["provider"] = "g5k"
    return env


@enostask(new=True)
def vagrant(config, force, env=None, **kwargs):
    conf = VagrantConf.from_dictionnary(config["vagrant"])
    provider = Enos_vagrant(conf)
    roles, networks = provider.init(force_deploy=force)
    env["config"] = config
    env["roles"] = roles
    env["networks"] = networks
    env["provider"] = "vagrant"
    return env


@enostask()
def inventory(**kwargs):
    env = kwargs["env"]
    roles = env["roles"]
    networks = env["networks"]
    env["inventory"] = os.path.join(env["resultdir"], "hosts")
    generate_inventory(roles, networks, env["inventory"], check_networks=True)


@enostask()
def prepare(**kwargs):
    env = kwargs["env"]
    extra_vars = {
        #'enos_playbook': 'holder',
        "enos_action": "prepare"
    }
    run_ansible([os.path.join(ANSIBLE_DIR, "site.yml")],
                env["inventory"], extra_vars=extra_vars)


@enostask()
def backup(**kwargs):
    env = kwargs["env"]
    extra_vars = {
        "enos_action": "backup"
    }
    run_ansible([os.path.join(ANSIBLE_DIR, "site.yml")],
                env["inventory"], extra_vars=extra_vars)


@enostask()
def destroy(**kwargs):
    env = kwargs["env"]
    extra_vars = {
        "enos_action": "destroy"
    }
    run_ansible([os.path.join(ANSIBLE_DIR, "site.yml")],
                env["inventory"], extra_vars=extra_vars)


PROVIDERS = {
    "g5k": g5k,
    "vmong5k": vmong5k,
    "vagrant": vagrant
    #    "virtual": virtual_layer
    #    "static": static
    #    "chameleon": chameleon
}


# g5k
# g5k_env
# scenario -> pm,vm,cont
# |-> inventory of necessary nodes with a new env

@enostask()
def reload_env(force, env=None, **kwargs):
    print(env)
    provider = env["provider"]
    env = PROVIDERS[provider](env["config"], force, env=env["resultdir"])
    return env


VCONSTRUCTORS = {
        "vm": VirtualMachine,
        "cont": VirtualMachine
        }

def get_mac(ip):
    mac =['00','16','3E']+['{:02X}'.format(int(i,2)) for i in IPAddress(ip).bits().split('.')[-3:]]
    return EUI(':'.join(mac), dialect=mac_unix_expanded)

def compute_wanted_resources(prev_env,config,spreading, **kwargs):
    from collections import defaultdict
    from netaddr import iter_iprange
    idx = 0
    roles = defaultdict(list)
    uinventory = []
    vinventory = []
    prev_roles = {k:(x for x in v) for k,v in prev_env["roles"].items()}
    g5k_network = [n for n in prev_env["networks"] 
                    if "my_subnet" in n["roles"]][0]
    #g5k_network["mac_start"] = get_mac(g5k_network["start"])
    #g5k_network["mac_end"] = get_mac(g5k_network["end"])

    euis = mac_range(EUI(g5k_network["mac_start"]), EUI(g5k_network["mac_end"]))
    for _ in range(kwargs["shift"]):
        next(euis)
    hierarchy = {}
    for u_type, v_type_list in spreading:
        pm = next(prev_roles[u_type])
        uinventory.append(pm)
        content = []
        for v_type in v_type_list:
            name = "virtual-{}-{}".format(
                    idx,
                    v_type)
            idx += 1
            curr_config = config[v_type]
            import re
            cons_type = re.match("([^0-9]*)",v_type).group()

            v = VCONSTRUCTORS[cons_type](
                    name, next(euis), 
                    {**(kwargs["default_flavour"]),**curr_config["flavour_desc"]}, 
                    pm)
            
            content.append({**v.to_dict(),"vtype":cons_type})
            v.vtype = cons_type
            vinventory.append(v)
            for role in curr_config["roles"]:
                roles[role].append(v)
        hierarchy[pm.alias]=content
    return dict(roles),dict(hierarchy), uinventory, vinventory

@enostask(new=True)
def virtual_layer(config, force, prev_env, spreading, env=None, swallow_force=False, **kwargs):
    '''
    config : current layer config
    force : reset every layers
    env : current env
    '''
    default_flavour = {
            "cores": 2,
            "mem": 2}
    if "cores" in kwargs: 
        imposed_flavour["cores"]=kwargs["cores"]
    if "mem" in kwargs: 
        imposed_flavour["mem"]=kwargs["mem"]
    # compute_wanted_resources with config (and params)
    roles, v_by_host, \
    uinventory, vinventory = \
        compute_wanted_resources(
            prev_env,config, spreading, 
            default_flavour=default_flavour, 
            shift=kwargs.get("shift",0))
    env["inventory"] = os.path.join(env["resultdir"],"vhosts")
    env["roles"] = {}
    for k,v in prev_env["roles"].items():
        env["roles"][k] = v
    for k,v in roles.items():
        if k not in env["roles"]:
            env["roles"][k] = v
        else:
            env["roles"][k] += v
   
    generate_inventory(env["roles"], prev_env["networks"], env["inventory"])
    
    vms_by_host = {k:[e for e in v if e["vtype"]=="vm"] for k,v in v_by_host.items()}
    cont_by_host = {k:[e for e in v if e["vtype"]=="cont"] for k,v in v_by_host.items()}

    g5k_subnet = [n for n in prev_env["networks"] 
                    if "my_subnet" in n["roles"]][0]
    extra_vars = {
        'cont': cont_by_host,
        'vms': vms_by_host,
        'enos_playbook': 'holder',
        'enos_action': 'deploy',
        'subnet': dict(g5k_subnet),
        'base_image': "/grid5000/virt-images/debian9-x64-std.qcow2"
            }
    run_ansible([os.path.join(ANSIBLE_DIR, "site.yml")], prev_env["inventory"], extra_vars)
    env["prev_env"] = prev_env
    env["config"] = config
    env["networks"] = prev_env["networks"]
    env["spreading"] = spreading
    env["provider"] = "virtual_layer"
    return env


@enostask()
def hanoi(**kwargs):
    env = kwargs["env"]
    extra_vars = {
        'location': kwargs.get("l"),
        'enos_playbook': 'experiment',
        "res_dir": env["resultdir"],
        "bench_id": str(uuid.uuid4()),
        "enos_action": "hanoi"
    }
    run_ansible([os.path.join(ANSIBLE_DIR, "site.yml")],
                env["inventory"], extra_vars=extra_vars)

@enostask()
def ping(**kwargs):
    env = kwargs["env"]
    extra_vars = {
        'target': kwargs.get("t"),
        'sources': kwargs.get("s"),
        'enos_playbook': 'experiment',
        'ping_options': '',
        "res_dir": env["resultdir"],
        "bench_id": str(uuid.uuid4()),
        "enos_action": "ping"
    }
    run_ansible([os.path.join(ANSIBLE_DIR, "site.yml")],
                env["inventory"], extra_vars=extra_vars)


@enostask()
def iperf(**kwargs):
    env = kwargs["env"]
    extra_vars = {
        'server': kwargs.get("s"),
        'clients': kwargs.get("c"),
        's_options': "",
        'c_options': "",
        'enos_playbook': 'experiment',
        "enos_action": "iperf",
        "res_dir": env["resultdir"],
        "bench_id": str(uuid.uuid4())
    }
    run_ansible([os.path.join(ANSIBLE_DIR, "site.yml")],
                env["inventory"], extra_vars=extra_vars)

@enostask()
def overcommit(**kwargs):
    env = kwargs["env"]
    extra_vars = {
        "res_dir": env["resultdir"],
        "bench_id": str(uuid.uuid4()),
        'enos_playbook': 'experiment',
        "enos_action": "overcommit"
    }
    run_ansible([os.path.join(ANSIBLE_DIR, "site.yml")],
                env["inventory"], extra_vars=extra_vars)
